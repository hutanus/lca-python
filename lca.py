# lca.py
#
# Contains functions to list trees and calculate their lowest common ancestors
import os

def getTrees():
    treeFiles = []

    for file in os.listdir('./res'):
        if(os.path.isfile('./res/' + file)):
            if(file.endswith('.json')):
                treeFiles.append('./res/' + file) 

    return treeFiles

def listNodes(tree):
    if tree == None:
        return ""
    
    name = tree['name'] + " "

    leftName = ""
    if 'left' in tree:
        leftName = listNodes(tree['left'])

    rightName = ""
    if 'right' in tree:
        rightName = listNodes(tree['right'])
    
    return "{}{}{}".format(name, leftName, rightName)

def isNodeInTree(tree, nodeName):
    if tree == None:
        return False

    return pathToNode(tree, nodeName) != []

def pathToNode(tree, nodeName):
    if tree == None:
        return []
    
    # Catch for empty tree
    if not "name" in tree:
        return False
    
    # Check if we've found the node
    if tree['name'] == nodeName:
        return [tree['name']]
    
    # Check the left subtree
    if "left" in tree and tree['left'] != {}:
        # accumulatingPath is the path from the root to the current node
        accumulatingPath = [tree['name']]
        # extension is the path from the current node to the target node
        extension = pathToNode(tree['left'], nodeName)
        accumulatingPath.extend(extension)

        # if there is a path from the current node to the target node
        if not extension == []:
            # send the path back up
            return accumulatingPath

    # Check the right subtree
    if "right" in tree and tree['right'] != {}:
        # accumulatingPath is the path from the root to the current node
        accumulatingPath = [tree['name']]
        # extension is the path from the current node to the target node
        extension = pathToNode(tree['right'], nodeName)
        accumulatingPath.extend(extension)

        # if there is a path from the current node to the target node
        if not extension == []:
            # send the path back up
            return accumulatingPath
    
    # No left or right subtrees, return []
    return []

def lowestCommonAncestor(tree, firstNode, secondNode):
    pathToFirstNode = pathToNode(tree, firstNode)
    pathToSecondNode = pathToNode(tree, secondNode)

    # Loop through the nodes in the path from the root to the first node until...
    commonIndex = 0
    for pathIndex in range(0, len(pathToFirstNode)):
        # we find a difference between the two paths
        if pathToFirstNode[pathIndex] != pathToSecondNode[pathIndex]:
            commonIndex = pathIndex - 1 if pathIndex - 1 >= 0 else 0
            break
    
    return pathToFirstNode[commonIndex]