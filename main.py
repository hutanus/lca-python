# main.py
#
# Handles the user interface

import json
import lca
import sys

# Get all the trees
trees = lca.getTrees()

# Show the user the available trees
print(str(len(trees)) + " tree files found:\n")
for treeIndex in range(0, len(trees)):
    print("[{}] in '{}'".format(treeIndex, trees[treeIndex]))

# Ask the user to select one
print()
selected = input('Enter the index of the tree to work with: ')

# Make sure the user selected a number between 0 and the number of trees
if not selected.isdigit() or not (int(selected) >= 0 and int(selected) < len(trees)):
    print("Couldn't find tree {}. Please enter a number between 0 and {}".format(selected, len(trees) - 1))
    sys.exit()

# Load the tree
tree = None
with open(trees[int(selected)]) as treeFile:
  tree = json.load(treeFile)

# Ask the user for the first node
print("\nSelected tree '{}' with nodes:".format(trees[int(selected)]))
print(lca.listNodes(tree))

firstNode = input("\nPlease select the first node: ")
if not lca.isNodeInTree(tree, firstNode):
    print("Couldn't find node {}".format(firstNode))
    sys.exit()

# Ask the user for the second node
secondNode = input("\nPlease select the second node: ")
if not lca.isNodeInTree(tree, secondNode):
    print("Couldn't find node {}".format(secondNode))
    sys.exit()

# Calculate the lowest common ancestor
lowestCommonAncestor = lca.lowestCommonAncestor(tree, firstNode, secondNode)
print("The lowest common ancestor between {} and {} is {}".format(firstNode, secondNode, lowestCommonAncestor))