import unittest
import lca
import json

class TestLCA(unittest.TestCase):


    ################################
    # Tests if the standard trees (full trees of depth 2 and 3) are located in the res folder
    ################################

    def testStandardTrees(self):
        results = lca.getTrees()
        self.assertTrue('./res/full_depth_2.json' in results)
        self.assertTrue('./res/full_depth_3.json' in results)

    
    #############################
    # Tests for lca.listnodes() #
    #############################

    # "List the nodes of an empty tree"
    def testListNodesNull(self):
        self.assertEqual(lca.listNodes(None), "")

    # Tests if the full_depth_2.json has the correct nodes
    def testListNodesFull2(self):
        tree = None
        with open('./res/full_depth_2.json') as treeFile:
            tree = json.load(treeFile)
        
        self.assertEqual(lca.listNodes(tree), 'A B C ')
    
    # Tests if the full_depth_3.json has the correct nodes
    def testListNodesFull3(self):
        tree = None
        with open('./res/full_depth_3.json') as treeFile:
            tree = json.load(treeFile)
        
        self.assertEqual(lca.listNodes(tree), 'A B D E C F G ')

    
    ################################
    # Tests for lca.isNodeInTree() #
    ################################

    # "Is some node in an empty tree?"
    def testIsNodeInEmptyTree(self):
        self.assertFalse(lca.isNodeInTree(None, 'A'))
    
    # "Is `None` in a tree?"
    def testIsNodeInEmptyTreeNoneNode(self):
        tree = None
        with open('./res/full_depth_3.json') as treeFile:
            tree = json.load(treeFile)
        
        self.assertFalse(lca.isNodeInTree(tree, None))

    # Tests if the isNodeInTree function finds nodes at levels 1, 2, and 3 in trees
    def testIsNodeInTreeFull3(self):
        tree = None
        with open('./res/full_depth_3.json') as treeFile:
            tree = json.load(treeFile)
        
        self.assertTrue(lca.isNodeInTree(tree, 'A'))
        self.assertTrue(lca.isNodeInTree(tree, 'B'))
        self.assertTrue(lca.isNodeInTree(tree, 'D'))
        self.assertTrue(lca.isNodeInTree(tree, 'F'))
    
    # Tests if the isNodeInTree function can handle nodes without its left or right children declared
    def testIsNodeInTreeUndeclared(self):
        tree = None
        with open('./res/test_files/no_left_or_right.json') as treeFile:
            tree = json.load(treeFile)

        self.assertFalse(lca.isNodeInTree(tree, 'B'))
        
    # Tests if the isNodeInTree function can handle nodes without its left or right children defined
    def testIsNodeInTreeUndefined(self):
        tree = None
        with open('./res/test_files/left_and_right_undefined.json') as treeFile:
            tree = json.load(treeFile)

        self.assertFalse(lca.isNodeInTree(tree, 'B'))
    

    ##############################
    # Tests for lca.pathToNode() #
    ##############################
    
    # Tests for the path to a node in an empty tree
    def testPathToNodeNoTree(self):
        self.assertEqual(lca.pathToNode(None, 'A'), [])

    # Tests for the paths to level one, two, and three nodes
    def testPathToNode(self):
        tree = None
        with open('./res/full_depth_3.json') as treeFile:
            tree = json.load(treeFile)

        self.assertEqual(lca.pathToNode(tree, 'A'), ['A'])
        self.assertEqual(lca.pathToNode(tree, 'B'), ['A', 'B'])
        self.assertEqual(lca.pathToNode(tree, 'D'), ['A', 'B', 'D'])
    
    # Tests for paths to non-existing nodes in a tree with undefined nodes
    def testPathToNull(self):
        tree = None
        with open('./res/test_files/left_and_right_undefined.json') as treeFile:
            tree = json.load(treeFile)

        self.assertEqual(lca.pathToNode(tree, 'B'), [])
        self.assertEqual(lca.pathToNode(tree, 'C'), [])

unittest.main()