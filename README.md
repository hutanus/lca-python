# Lowest Common Ancestor (Python)


This is my repository for solving the lowest common ancestor for CSU33012. This version is written in python, a language I like for quickly prototyping solutions to problems because of its readability and libraries.

# Running lca-python

All you need to run this is python (I used version 3)

To run the application:
>python3 main.py

To run the tests
>python3 test_lca.py

## Tree files

I've included two tree files in the res folder.

* `full_depth_2.json` is a tree with root node "A" and two child nodes "B" and "C".
* `full_depth_3.json` is identical to `full_depth_2.json` but with another level below "B" and "C".

In addition to these two files, I've also included some malformed files (for testing purposes) under `test_files`

* `left_and_right_undefined.json` is a tree with a root node "A" with child nodes "left" and "right" set to empty objects
* `no_left_or_right.json` is a tree with a root node "A" with neither "left" nor "right" defined.

## Algorithm

The lowest common ancestor is calculated by comparing the paths between two nodes from the root. For example, the lowest common ancestor between C and D is B if their paths are ABC and ABD

## Repository structure

* The `res` folder contains sample trees (in JSON format)
* `main.py` handles the user interface
* `lca.py` calculates the lowest common ancestor
* `test_lca.py` tests `lca.py`